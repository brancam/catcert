CatCert
=======

This library allows PHP developers to certify documents against the CatCert platform through an object oriented solution.

Examples of use
===============

Getting the hash of a document
------------------------------

:::php
<?php

$catcert = new CatCert();
$hash = $catcert->doHash($documentUsuallyInXml);

Sealing a document
------------------

:::php
<?php

$catcert = new CatCert();
$result = $catcert->doSeal($documentUsuallyInXml);

