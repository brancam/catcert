<?php
namespace Perception\Library\CatCert;

use Perception\Library\CatCert\Exception\ConnectionException;
use Perception\Library\CatCert\Exception\InvalidSignatureException;

class CatCert
{
    protected $xmlTemplate = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Body>
        <dss:SignRequest Profile="urn:oasis:names:tc:dss:1.0:profiles:timestamping"
                         xmlns:dss="urn:oasis:names:tc:dss:1.0:core:schema">
            <dss:OptionalInputs>
                <dss:KeySelector>
                    <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
                        <ds:X509Data>
                            <ds:X509Certificate>
                                MIIINTCCBx2gAwIBAgIQcCe8TntROdxHjKekHM7mqzANBgkqhkiG9w0BAQUFADCCATExCzAJBgNVBAYTAkVTMTswOQYDVQQKEzJBZ2VuY2lhIENhdGFsYW5hIGRlIENlcnRpZmljYWNpbyAoTklGIFEtMDgwMTE3Ni1JKTE0MDIGA1UEBxMrUGFzc2F0Z2UgZGUgbGEgQ29uY2VwY2lvIDExIDA4MDA4IEJhcmNlbG9uYTEuMCwGA1UECxMlU2VydmVpcyBQdWJsaWNzIGRlIENlcnRpZmljYWNpbyBFQ1YtMjE3MDUGA1UECxMuVmVnZXUgaHR0cHM6Ly93d3cuY2F0Y2VydC5uZXQvdmVyQ0lDLTIgICAoYykwMzE0MDIGA1UECxMrU2VjcmV0YXJpYSBkJ0FkbWluaXN0cmFjaW8gaSBGdW5jaW8gUHVibGljYTEQMA4GA1UEAxMHRUMtU0FGUDAeFw0wODAxMTUxMjMxMzJaFw0xMjAxMTUxMjMxMjVaMIIBgjELMAkGA1UEBhMCRVMxPzA9BgNVBAoUNkNvbnNvcmNpIEFkbWluaXN0cmFjafMgT2JlcnRhIEVsZWN0cvJuaWNhIGRlIENhdGFsdW55YTEaMBgGA1UECxQRRGlyZWNjafMgZ2Vy6G5jaWExMTAvBgNVBAsUKFNlcnZlaXMgUPpibGljcyBkZSBDZXJ0aWZpY2FjafMgQ0VJWFNBLTExPDA6BgNVBAsTM1ZlZ2V1IGh0dHBzOi8vd3d3LmNhdGNlcnQubmV0L3ZlckNFSVhTQS0xU0FGUCAoYykwNzEWMBQGA1UEBBMNT2xpdmFyZXMgT2JpczEUMBIGA1UEKhMLSm9hbiBBbnRvbmkxEjAQBgNVBAUTCVEwODAxMTc1QTFIMEYGA1UEAxQ/Q0VJWFNBLTEgQ29uc29yY2kgQWRtaW5pc3RyYWNp8yBPYmVydGEgRWxlY3Ry8m5pY2EgZGUgQ2F0YWx1bnlhMRkwFwYKKwYBBAGBkxYBARMJNzcyODIxMzFTMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDO3zdwWn/EzpMiQshvu7EKq71SJwT6GWyyHSzqgkDZE6EyzUEyDYkRk/qbfjpp8khyNFsngmDpu8VIsh6taypFoIWZOrPImosa3zmgtCwqTgxCjLBMiRVJNfwNAj/Pil7EmFvawJH+8Zc5j0CmEX+0ZRfa+xSERYurf38i6fmiqQIDAQABo4IDdjCCA3IwHgYDVR0SBBcwFYETZWNfc2FmcEBjYXRjZXJ0Lm5ldDA3BgNVHREEMDAugRRqYW9saXZhcmVzQGFvY2F0Lm5ldKQWMBQxEjAQBgNVBAUTCVEwODAxMTc1QTAOBgNVHQ8BAf8EBAMCBPAwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMBEGCWCGSAGG+EIBAQQEAwIFoDAdBgNVHQ4EFgQUtOMhzQI6shNhIHAr1JLMGoOaSQ8wggEnBgNVHSMEggEeMIIBGoAUBbftTxZS3frmksCicnlWYFTCJBOhge+kgewwgekxCzAJBgNVBAYTAkVTMTswOQYDVQQKEzJBZ2VuY2lhIENhdGFsYW5hIGRlIENlcnRpZmljYWNpbyAoTklGIFEtMDgwMTE3Ni1JKTEuMCwGA1UECxMlU2VydmVpcyBQdWJsaWNzIGRlIENlcnRpZmljYWNpbyBFQ1YtMTE2MDQGA1UECxMtVmVnZXUgaHR0cHM6Ly93d3cuY2F0Y2VydC5uZXQvdmVyQ0lDLTEgIChjKTAzMSEwHwYDVQQLExhHZW5lcmFsaXRhdCBkZSBDYXRhbHVueWExEjAQBgNVBAMTCUVDLUdFTkNBVIIQb+8vFPRPzN8+HCEWmIwVNjCB1QYDVR0gBIHNMIHKMIHHBg0rBgEEAfV4AQMBgSEBMIG1MDMGCCsGAQUFBwIBFidodHRwczovL3d3dy5jYXRjZXJ0Lm5ldC92ZXJDRUlYU0EtMVNBRlAwfgYIKwYBBQUHAgIwchpwQXF1ZXN0IOlzIHVuIGNlcnRpZmljYXQgcmVjb25lZ3V0IGRlIHBlcnNvbmEganVy7WRpY2EgZGUgY2xhc3NlIDEuIFZlZ2V1IGh0dHBzOi8vd3d3LmNhdGNlcnQubmV0L3ZlckNFSVhTQS0xU0FGUDAzBggrBgEFBQcBAQQnMCUwIwYIKwYBBQUHMAGGF2h0dHA6Ly9vY3NwLmNhdGNlcnQubmV0MBgGCCsGAQUFBwEDBAwwCjAIBgYEAI5GAQEwZAYDVR0fBF0wWzBZoFegVYYoaHR0cDovL2Vwc2NkLmNhdGNlcnQubmV0L2NybC9lYy1zYWZwLmNybIYpaHR0cDovL2Vwc2NkMi5jYXRjZXJ0Lm5ldC9jcmwvZWMtc2FmcC5jcmwwDQYJKoZIhvcNAQEFBQADggEBADRCioYHqgISnYtyWUhcWeeTJVNyCLmfwWPlO/70z318QwPn7g5W2tPNImDH8PE8cPbJPW6Yr434jOsTKSYR4HcZCgKNeOdx1v6rRF86w1enWo/7V70fTkygzhckKjzWrcWgV/hHmjOdqyTmK9RxQUtoYbK8H7DH1Kue2jXT/J+esKRIIMSt6kNCTQdefd5sAzaMSjHxu5/bCGzWRvlkGXgJf5up9noa0O9P4kqEq92aMFHaik6gJibPWfnga7ZY1EeqRKplOfTSpbVY+yIzf/Cwvusy2nmhXMkOjxBrEUs+2GuX1pgxZVw3NyYWx+cQUF8cNlftoGkdIVTRN+e1dOY=
                            </ds:X509Certificate>
                        </ds:X509Data>
                    </ds:KeyInfo>
                </dss:KeySelector>
                <dss:SignatureType>oasis:names:tc:dss:1.0:core:schema:XMLTimeStampToken</dss:SignatureType>
                <dss:IncludeObject ObjId="Doc1" WhichDocument="Doc1" hasObjectTagsAndAttributesSet="false"
                                   createReference="true"/>
            </dss:OptionalInputs>
            <dss:InputDocuments>
                <dss:DocumentHash ID="Doc1">
                    <ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"
                                     xmlns:ds="http://www.w3.org/2000/09/xmldsig#"/>
                    <ds:DigestValue xmlns:ds="http://www.w3.org/2000/09/xmldsig#">__REPLACE_THIS_TEXT__</ds:DigestValue>
                </dss:DocumentHash>
            </dss:InputDocuments>
        </dss:SignRequest>
    </soapenv:Body>
</soapenv:Envelope>
EOT;

    public function doSeal($string)
    {
        // Apply the hash to the xml template
        $hash = $this->doHash($string);
        $request = str_ireplace('__REPLACE_THIS_TEXT__', $hash, $this->xmlTemplate);

        // POST PSIS to validate the certificate
        $response = $this->doPostRequest("http://psis.catcert.net/psis/catcert/dss", $request);

        if (strchr($response, "Signature created") == false) {
            throw new InvalidSignatureException("An error ocurred generating the signature, $response");
        }

        return $response;
    }

    public function doHash($string)
    {
        return base64_encode(sha1($string, true));
    }

    private function doPostRequest($url, $data)
    {
        $params = array('http' => array(
            'method' => 'POST',
            'content' => $data
       ));

        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);

        if (!$fp) {
            throw new ConnectionException("An error ocurred opening $url, $php_errormsg");
        }

        $response = @stream_get_contents($fp);
        if ($response === false) {
            throw new ConnectionException("An error ocurred opening reading data from $url, $php_errormsg");
        }

        return $response;
    }
}